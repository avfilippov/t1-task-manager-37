package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.repository.ISessionRepository;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.ISessionService;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.model.Session;
import ru.t1.avfilippov.tm.repository.SessionRepository;

import static ru.t1.avfilippov.tm.constant.TestData.SESSION;
import static ru.t1.avfilippov.tm.constant.TestData.USER1;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final ISessionService service = new SessionService(connectionService);


    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException {
        service.add(SESSION);
    }

    @After
    public void after() throws UserIdEmptyException {
        service.clear(USER1.getId());
    }

    @Test
    public void add() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(SESSION));
        Assert.assertThrows(Exception.class, () -> service.add((Session) null));
    }

    @Test
    public void addByUserId() throws UserIdEmptyException, ProjectNotFoundException {
        Assert.assertNotNull(service.add(USER1.getId(), SESSION));
    }

    @Test
    public void createByUserId() {
        Assert.assertEquals(SESSION.getUserId(),USER1.getId());
    }

    @Test
    public void findAllNull() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        @NotNull final SessionService SessionService = new SessionService(connectionService);
        Assert.assertTrue(SessionService.findAll().isEmpty());
    }

    @Test
    public void updateByNullId() {
        Assert.assertNull(service.findOneById(USER1.getId(),null));
    }

}
