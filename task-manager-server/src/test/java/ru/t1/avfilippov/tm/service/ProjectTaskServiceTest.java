package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.api.repository.IProjectRepository;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.exception.AbstractException;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.repository.ProjectRepository;
import ru.t1.avfilippov.tm.repository.TaskRepository;

import java.util.Objects;

import static ru.t1.avfilippov.tm.constant.TestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @Nullable
    private final IProjectRepository projectRepository = new ProjectRepository(connectionService.getConnection());

    @Nullable
    private final ITaskRepository taskRepository = new TaskRepository(connectionService.getConnection());

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Before
    public void before() throws UserIdEmptyException, ProjectNotFoundException {
        projectRepository.add(USER1.getId(),USER_PROJECT1);
        projectRepository.add(USER1.getId(),USER_PROJECT2);

        taskRepository.add(USER1.getId(),USER_TASK1);
        taskRepository.add(USER1.getId(),USER_TASK2);
    }

    @After
    public void after() throws UserIdEmptyException {
        projectRepository.clear(USER1.getId());
        taskRepository.clear(USER1.getId());
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        projectTaskService.bindTaskToProject(USER1.getId(),USER_PROJECT1.getId(),USER_TASK1.getId());
        Assert.assertTrue(Objects.equals(USER_TASK1.getProjectId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeProjectById() throws AbstractException {
        projectTaskService.removeProjectById(USER1.getId(),USER_PROJECT1.getId());
        Assert.assertFalse(projectRepository.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER1.getId(),USER_PROJECT1.getId(),USER_TASK1.getId());
        Assert.assertNull(USER_TASK1.getProjectId());
    }

}
