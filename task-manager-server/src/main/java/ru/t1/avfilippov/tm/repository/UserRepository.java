package ru.t1.avfilippov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.IUserRepository;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final static String TABLE = "tm.tm_user";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return TABLE;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("ID"));
        user.setLogin(row.getString("LOGIN"));
        user.setEmail(row.getString("EMAIL"));
        user.setRole(Role.valueOf(row.getString("ROLE")));
        user.setFirstName(row.getString("FIRST_NAME"));
        user.setLastName(row.getString("LAST_NAME"));
        user.setMiddleName(row.getString("MIDDLE_NAME"));
        user.setPasswordHash(row.getString("PASSWORD"));
        user.setLocked(row.getBoolean("LOCKED"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, email, role, first_name, last_name, middle_name, password, locked)"
                        + "VALUES (?,?,?,?,?,?,?,?,?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getEmail());
            statement.setString(4, model.getRole().toString());
            statement.setString(5, model.getFirstName());
            statement.setString(6, model.getLastName());
            statement.setString(7, model.getMiddleName());
            statement.setString(8, model.getPasswordHash());
            statement.setBoolean(9, model.getLocked());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET role = ?, first_name = ?, last_name = ?, middle_name = ?, password = ?, locked = ? WHERE id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getRole().toString());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getMiddleName());
            statement.setString(5, user.getPasswordHash());
            statement.setBoolean(6, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

}
