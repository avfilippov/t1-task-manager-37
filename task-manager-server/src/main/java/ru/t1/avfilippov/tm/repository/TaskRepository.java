package ru.t1.avfilippov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    private final static String TABLE = "tm.tm_task";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || projectId == null) return Collections.emptyList();
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %S WHERE USER_ID = ? AND PROJECT_ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    protected String getTableName() {
        return TABLE;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("ID"));
        task.setName(row.getString("NAME"));
        task.setDescription(row.getString("DESCRIPTION"));
        task.setUserId(row.getString("USER_ID"));
        task.setStatus(Objects.requireNonNull(Status.toStatus(row.getString("STATUS"))));
        task.setCreated(row.getTimestamp("CREATED"));
        task.setProjectId(row.getString("PROJECT_ID"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id,created,name,description,status,user_id, project_id)"
                        + "VALUES (?,?,?,?,?,?,?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, Status.NOT_STARTED.name());
            statement.setString(6, model.getUserId());
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public @NotNull Task add(@Nullable final String userId, @NotNull final Task model) {
        if (userId == null) throw new UserIdEmptyException();
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, description = ?, status = ?, project_id = ? WHERE id = ?", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

}
