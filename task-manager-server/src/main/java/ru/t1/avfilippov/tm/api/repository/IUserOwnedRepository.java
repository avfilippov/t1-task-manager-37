package ru.t1.avfilippov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    long getSize(@Nullable String userId);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M add(@Nullable String userId, @NotNull M model);

    void removeAll(@Nullable String userId);

}
