package ru.t1.avfilippov.tm.api.repository;

import ru.t1.avfilippov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session>, IRepository<Session> {

}
