package ru.t1.avfilippov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.repository.ISessionRepository;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.ISessionService;
import ru.t1.avfilippov.tm.model.Session;
import ru.t1.avfilippov.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService{

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

}
