package ru.t1.avfilippov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.IRepository;
import ru.t1.avfilippov.tm.comparator.CreatedComparator;
import ru.t1.avfilippov.tm.comparator.StatusComparator;
import ru.t1.avfilippov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("DELETE FROM %S", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %S", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %S ORDER BY %S", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        @NotNull final List<M> result = new ArrayList<>();
        for (M model :models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %S WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return findAll().get(index);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %S", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
                resultSet.next();
                return resultSet.getLong("COUNT");
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        @NotNull final String sql = String.format("DELETE FROM %S WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) {
        @NotNull final M model = findOneById(id);
        @NotNull final String sql = String.format("DELETE FROM %S WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        for (M model: collection){
            remove(model);
        }
    }

}
