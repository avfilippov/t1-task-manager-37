package ru.t1.avfilippov.tm.repository;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.ISessionRepository;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.UUID;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    private final static String TABLE = "tm.tm_session";

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return TABLE;
    }

    @Override
    @NotNull
    @SneakyThrows
    protected  Session fetch(@NotNull final ResultSet row) {
        System.out.println("Fetching Started");
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setDate(row.getTimestamp("date"));
        session.setUserId(row.getString("user_id"));
        session.setRole(Role.valueOf(row.getString("role")));
        return session;
    }

    @Override
    @NotNull
    @SneakyThrows
    public  Session add(@NotNull final Session model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, date, user_id, role)"
                        + "VALUES (?,?,?,?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getDate().getTime()));
            statement.setString(3, model.getUserId());
            statement.setString(4, model.getRole().toString());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @NotNull
    public  Session add(@Nullable final String userId, final @NotNull Session model) {
        if (userId == null) throw new UserIdEmptyException();
        model.setUserId(userId);
        return add(model);
    }

}

